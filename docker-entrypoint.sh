#!/bin/bash

# unregister the runner from the group / project
unregister_runner_manager() {
    curl --request DELETE "$1/api/v4/runners" --form "token=$2"
}

# register the runner in the project or group
register_runner_manager() {

    # runner manager name /w date (appears in runners list)
    runner_id="RUNNER_MANAGER_$(date +%s)"

    # uses the GITLAB_REGISTRATION_TOKEN Variable to register the runner
    result_json=$(
        curl --request POST "$1/api/v4/runners" \
            --form "token=$2" \
            --form "description=${runner_id}" \
            --form "tag_list=$3" \
            --form "run_untagged=false"
    )

    runner_token=$(echo $result_json | jq -r '.token')

    # create the config from template
    export RM_IDENTIFICATION=$runner_id
    export RM_TOKEN=$runner_token
    envsubst < /tmp/config_runner_manager_temp.toml > /etc/gitlab-runner/config.toml
}

# create the GitLab Runner Worker Config based on the template config_driver_temp.toml
create_executor_config() {
    envsubst < /tmp/config_driver_temp.toml > /etc/gitlab-runner/config_driver.toml
}

create_executor_config

register_runner_manager ${GITLAB_URL} ${GITLAB_REGISTRATION_TOKEN} ${RM_TAGS}

gitlab-runner run

unregister_runner_manager ${GITLAB_URL} ${runner_token}
