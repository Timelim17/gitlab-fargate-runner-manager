GitLab Runner Manager Image für Task Definition

Benötigte Umgebungsvariablen: 
- GITLAB_URL: URL für das zu verwendende GitLab
- GITLAB_REGISTRATION_TOKEN: GitLab Registrierungs Token für die Registrierung in einer Gruppe / einem Projekt
- CLUSTER_NAME: Name des Clusters in dem die Worker Container erstellt werden
- FARGATE_REGION: AWS Fargate Region
- FARGATE_SUBNET: Subnet für die Container
- SECURITY_GROUP: Security Group (benötigt ssh für die Kommunikation zwischen Manager und Worker)
- WORKER_TASK_DEFINITION: AWS ECS Task-Name für den Worker
- RM_TAGS: Tags für den Runner Manager (Dient vorerst der Eingrenzung: Welche Jobs vom Serverless Prototypen übernommen werden) 
